# shoppingcart-cache-demo
This is a demo application to demonstrate Caching using spring-boot and redis.

Tech-stack:
- Spring Boot
- Redis [ make sure to change the config in application.properties file before trying to run the application]
- H2
- Spring-JPA
