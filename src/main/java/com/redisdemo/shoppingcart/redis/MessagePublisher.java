package com.redisdemo.shoppingcart.redis;

public interface MessagePublisher {

    void publish(final String message);
}
