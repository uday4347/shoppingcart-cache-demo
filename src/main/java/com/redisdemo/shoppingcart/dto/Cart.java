package com.redisdemo.shoppingcart.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "carts", schema = "shopping_cart_demo")
public class Cart {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
}
