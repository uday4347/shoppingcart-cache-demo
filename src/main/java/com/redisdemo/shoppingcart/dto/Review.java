package com.redisdemo.shoppingcart.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "reviews", schema = "shopping_cart_demo")
public class Review {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
    private int userId;
    private String reviewText;
}
