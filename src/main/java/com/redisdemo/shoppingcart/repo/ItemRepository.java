package com.redisdemo.shoppingcart.repo;

import com.redisdemo.shoppingcart.dto.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Integer> {
}
