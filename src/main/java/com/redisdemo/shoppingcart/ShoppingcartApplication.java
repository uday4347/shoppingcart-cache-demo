package com.redisdemo.shoppingcart;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.redisdemo.shoppingcart.dto.Item;
import com.redisdemo.shoppingcart.repo.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@EnableCaching
@SpringBootApplication
@EnableJpaRepositories
@ComponentScan("com.redisdemo.shoppingcart")
public class ShoppingcartApplication {

	@Autowired
	ItemRepository itemRepository;

	public static void main(String[] args) {
		SpringApplication.run(ShoppingcartApplication.class, args);
	}


	@Bean
	CommandLineRunner runner() {
		return args -> {
			// read json and write to db
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Item>> typeReference = new TypeReference<List<Item>>(){};
			InputStream inputStream = TypeReference.class.getResourceAsStream("/data/items.json");
			try {
				List<Item> items = mapper.readValue(inputStream,typeReference);
				itemRepository.saveAll(items);
				System.out.println("Items Saved!");
			} catch (IOException e){
				System.out.println("Unable to save Items: " + e.getMessage());
			}
		};
	}
}
