package com.redisdemo.shoppingcart.controller;

import com.redisdemo.shoppingcart.dto.Item;
import com.redisdemo.shoppingcart.repo.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/items")
public class ItemsController {

    @Autowired
    ItemRepository itemRepository;

    @Cacheable(value="items", key = "#root.methodName")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Item> getItems(){
        System.out.println("Hitting DB");
        //return new ArrayList<Item>(itemRepository.findAll().subList(0, 10));
        return itemRepository.findAll();
    }

    @CacheEvict(value = "items", allEntries = true)
    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addNewItem(@RequestBody Item item) {

    }
}
